<?php

namespace Drupal\mercury_editor_content_lock\Cache;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\content_lock\ContentLock\ContentLock;
use Drupal\Core\Cache\Context\RouteCacheContext;
use Drupal\mercury_editor\MercuryEditorTempstore;

/**
 * Determines if an entity is being viewed in Mercury Editor content lock.
 */
class NodeUsingLayoutParagraphsCacheContext extends RouteCacheContext {

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\mercury_editor\MercuryEditorTempstore $tempstore
   *   The tempstore service.
   */
  public function __construct(RouteMatchInterface $route_match, protected MercuryEditorTempstore $tempstore) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Mercury Editor content lock');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if ($uuid = $this->routeMatch->getParameter('uuid')) {
      $entity = $this->tempstore->get($uuid);
    }
    if (empty($entity)) {
      return 'is_mercury_editor_node_using_layout_paragraphs.0';
    }
    if (empty($entity->lp_storage_keys)) {
      return 'is_mercury_editor_node_using_layout_paragraphs.1';
    }
    return 'is_mercury_editor_node_using_layout_paragraphs.2';
  }

}
