(function (Drupal, drupalSettings) {
  Drupal.behaviors.mercuryEditorContentLock = {
    attach: function (context, settings) {

      // Insert the unlock button if content is locked and the current user
      // has access to break the lock.
      if (
        !document.querySelector('#me-unlock-btn') &&
        drupalSettings.mercury_editor_content_lock &&
        drupalSettings.mercury_editor_content_lock.break_lock_url
      ) {
        const unlockBtn = document.createElement('a');
        unlockBtn.classList.add('me-button--secondary');
        unlockBtn.setAttribute('id', 'me-unlock-btn');
        unlockBtn.setAttribute('href', '#');
        unlockBtn.innerHTML = 'Unlock';
        const doneBtn = document.querySelector('#me-done-btn');
        if (doneBtn) {
          doneBtn.parentElement.insertBefore(unlockBtn, doneBtn);
        }
        unlockBtn.setAttribute('href', drupalSettings.mercury_editor_content_lock.break_lock_url);
      }

      // Show a dialog if content is locked and the current user does not own
      // the lock.
      if (
        !document.querySelector('#mercury-content-lock-dialog') &&
        drupalSettings.mercury_editor_content_lock &&
        drupalSettings.mercury_editor_content_lock.message &&
        !drupalSettings.mercury_editor_content_lock.owns_lock
      ) {

        const p = document.createElement('p');
        p.textContent = drupalSettings.mercury_editor_content_lock.message;

        const dialogElement = document.createElement('mercury-dialog');
        dialogElement.setAttribute('id', 'mercury-content-lock-dialog');
        dialogElement.setAttribute('hide-close-button', '');
        dialogElement.appendChild(p);
        document.body.appendChild(dialogElement);

        const saveButton = document.getElementById('me-save-btn');
        if (saveButton) {
          saveButton.setAttribute('disabled', 'disabled');
        }

        const buttons = [
          {
            text: Drupal.t('Exit'),
            click: function () {
              // Get the destination query parameter.
              const urlParams = new URLSearchParams(window.location.search);
              let destination = urlParams.get('destination');
              if (!destination) {
                destination = drupalSettings.mercury_editor_content_lock.exit_url;
              }
              window.location.href = destination;
            },
            class: 'button',
          },
        ];

        if (drupalSettings.mercury_editor_content_lock.break_lock_url) {
          buttons.unshift({
            text: Drupal.t('Break lock'),
            title: Drupal.t('Break content lock to allow editing.'),
            click: function () {
              window.location.href = drupalSettings.mercury_editor_content_lock.break_lock_url;
            },
            class: 'button button--primary',
          });
        }

        const dialog = Drupal.mercuryDialog(dialogElement, {
          hideCloseButton: true,
          title: Drupal.t('Content is locked'),
          buttons
        });

        dialog.showModal();
      }
    }
  }
})(Drupal, drupalSettings);
